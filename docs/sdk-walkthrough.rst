======================
Unity SDK Walkthrough
======================

:SDK Version: 2.0.6
:Oldest Supported version of Unity: 2018.4

Below you’ll find our handy SDK walkthrough - a step-by-step guide to using the AdInMo SDK with your game project within Unity. Further monetization tips and information including payments can be found here on our `main website <https://www.adinmo.com/>`_.

.. contents:: Table of Contents
    :depth: 2

Register with AdInMo
***************************

To begin using the AdInMo SDK, an AdInMo developer account is required.

1. Go to `AdInMo Website <https://www.adinmo.com/>`_
2. Click **Login/Sign Up button**, top-right of the page.
3. Sign up using your **email** or with your **Google account**.
4. Fill in the appropriate details and confirm your account using the instructions given. If signing up via email you will be asked to confirm your account.

Portal: Creating a Game
***************************

A **Game** is a representation of your game within AdInMo.
During this process you will create a **Game Key** that is required to work with the AdInMo SDK, **please keep this information private**.

1. `Login <https://portal.adinmo.com/#/login>`_ to your developer account on AdInMo.
2. Click the **+ Add New Game** button.
3. Fill in the details of your game - these details can be edited at any time - and click **Save**.
4. Each game has its own **game key**, this can be viewed on the details page of a game. **You will need this game key later on in the walkthrough**.
5. To view the details of your game, simply click the name of the game you want to view, either on the overview or game list page.

Portal: Creating a Placement
*******************************

This part requires at least one game on the portal.

**Placements** are simply an object which will display an advertisement, they must contain a unity 2D texture. Placements can be grouped together to show the same advertisement across different placements. One game can support multiple placements.

During this process you will create a **Placement Key** that is required to work with the AdInMo SDK, **please keep this information private**.

1. View the game you wish to create a placement for on the portal.
2. Click the, **+ Add New Placement** button, if you already have any placements they can be viewed here.
3. Fill out the details of your placement and select an aspect ratio. Placements can be edited at any time.
4. To create a group, click **New Group**. Once a group has been created it can be edited by clicking **Edit Group**. By default, a placement is set to have no group.
5. Each placement has its own **placement key**, this can be found on the placement list. **You will need this placement key later on in the walkthrough**.

Download AdInMo SDK
***************************

:Oldest Supported version of Unity: 2018.4

Release notes for the AdInMo SDK can be found `here <https://documentation.adinmo.com/en/latest/release-note.html>`_.

**When updating an older version of the AdInMo SDK the best practice is to delete the existing AdInMo folder in assets before importing the latest version of the SDK.**

1. Navigate to the `SDK Download <https://www.adinmo.com/sdk-download-new/>`_ page.
2. Click Download SDK.
3. Create or open a project within Unity Editor. If downloading from the Unity Asset Store, you can either import from the package manager or through the asset store tab depending on the unity version.
4. From the top menu, click **Assets**, **Import Package**, **Custom Package**. 
5. Select the unity package you downloaded in step 2.
6. Ensure all the items are selected and click **Import**.
7. You should now see a folder called **Adinmo** displayed in the Unity Assets window.

Unity Editor: AdinmoManager
*****************************

A **game key** is required for this part.

First, the **AdinmoManager** needs to be added to the scene. Next, copy and paste the **game key** of your game into the **AdinmoManager**.

1. From the Assets window, select **Adinmo**, **Prefabs**.
2. Click and drag the prefab called **Adinmo Manager** into the scene hierarchy.
3. **If you are working on a development build, please check this box on the Adinmo Manager.**
4. Navigate back to the portal and to the appropriate game.
5. View the details of the game on the portal and copy the game key (starts with **gk…**).
6. In Unity Editor, paste the copied **game key** in the AdinmoManager in the **game key** field.

Unity Editor: Adding Placements
*********************************

Game Object Templates
======================
**Placement key** is required for this part.

AdInMo SDK 2.0 supports game objects templates (**image**, **quad** and **sprites**) of different aspect ratios that you can add to your scene without the need to manually create them.

1. Go to the **AdInMo** folder in the assets window.
2. Select **prefabs**.
3. From here, you can select the appropriate game folder for the game object you want to add. Each game object is named with its aspect ratio.

These templates already have the AdInMo Texture component added to them. If you want to create your own game object from scratch see the next section on how to add the AdInMo Texture component.


Unity Editor: AdinmoTexture Component
=============================================

**Placement key** is required for this part.

The AdInMo texture component on the game object that will act as the advertising placement within your game.

1. Create a game object, either an **image**, **quad** or **sprite**.
2. Select the game object in the scene and click **Add Component** in the inspector window.
3. In the search box, type and select **Adinmo Texture**.
4. Paste the placement key of the placement from the portal into the Adinmo Texture component, Placement keys start with **pk...**

User Consent GDPR
******************
In order to comply with GDPR user consent for the data to be collected is required and the consent request must be set by you. You can control if personal data is being collected using three methods via the SDK’s AdinmoManager:

.. code-block:: csharp
  :linenos:

  bool GetDataUseConsent()
  bool IsDataUseConsentSet()
  void SetDataUseConsent(bool consent)



Finished!
***************************

Run your game and advertisements will appear in your game.

For support, please check our `FAQ <https://www.adinmo.com/developer-faq-new/>`_.

Please Note!
############

**You will not be able to see impressions using a development build, uncheck this option if you are building a release.**

Unity Editor also has its own development build option under **File**, **Build Settings**. Ensure the **Development Build** option is not checked when building for release.

