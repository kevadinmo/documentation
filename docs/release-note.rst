===================
Unity Release Notes
===================

Here you will find a full list of release notes for the AdInMo SDK. To download the most recent version of the AdInMo SDK, `click here <https://www.adinmo.com/sdk-download-new/>`_ .


Version 2.0.12 (30/11/2021)
###########################

* Reduced network traffic
* Supports up to high Managed Code Stripping
* Reliability Improvements

.. contents:: Full List
    :depth: 3

Version 2.0.9 (10/12/2021)
###########################

* Incorrect placement key alert
* Updates to editor to show impression fail reason
* Number culture issue fixed
* Updates to debugging tools including updates to manager screen
* Added tooltips

Version 2.0.8 (10/6/2021)
###########################

* Performance updates and direct campaign tracking added

Version 2.0.6 (09/20/2021)
###########################

* Bug fixes and performance improvements.

Version 2.0.3 (09/03/2021)
###########################

* Bug fixes and performance improvements.
* Layer filtering to exclude layers from occlusion tests.

Version 2.0.2 (08/11/2021)
###########################

* Bug fixes and performance improvements.
* Improved advertisement filtering.

Version 2.0.1 (07/28/2021)
###########################

* Bug fixes and performance improvements.
* Improved advertisement filtering.

Version 2.0.0 (07/06/2021)
###########################

**Added:**

* Fill failure reasons to the management panel.
* Test for axis orientation to indicate to the developer with a warning when the local axis is not orientated correctly with the quad face.
* Local html rendering support for adverts.
* Created mesh, sprite and image prefabs for various aspect ratios.
* Created a management panel which summarises placements and gives details of coverages.

**Removed:**

* Developer build setting.
* Dialog feature.

**General:**

* Support for border colour as a background colour for html rendering.
* Messages to servers optimised for bandwidth and speed of compilation.
* Implemented occlusion and angle to camera for sampling images.
* Impression countdown progress bar.
* Image selection now works on a weighted system, where some ads are more likely to appear than others.
* Images cycling now occurs after an impression is made, or a period of time has elapsed.
* Outstanding impressions are now saved on application exit.

Version 1.5
############

Version 1.5.2 (02/10/2021)
***************************

- Startup time improvements, pause reduction.
- Improve image cache performance.
- Better, more careful logging.
- Remove unused textures in more cases.
- Fixed bugs in logic when there are no image candidates available for some reason.

Version 1.5.0 (01/12/2021)
***************************************

- Improve image cache cleanup logic.
- Make downloaded textures non-readable, which further improves the memory usage.
- Other minor fixes.

Version 1.4
###########################

Version 1.4.11 (12/29/2020)
***************************************

- Fixed a corner case upon old textures cache cleanup.
- Improved error handling in a few cases.
- Other minor stability improvements.

Version 1.4.10  (12/16/2020)
***************************************

- Hotfix for image cache duplicate key issue.


Version 1.4.9  (07/04/2020)
***************************************

- Performance updates.



Version 1.4.8  (11/25/2019)
***************************************

- Only allow one AdinmoManager to exist in the scene. Duplicates are auto-destroyed.


Version 1.4.7 (11/11/2019)
***************************************

- Properly handle AdInMoTextures getting destroyed.


Version 1.4.6 (10/9/2019)
***************************************

- Fixed frame rate hitches during Sprite. Create() at a app load time or when a texture cycles for the first time.


Version 1.4.5 (10/5/2019)
***************************************

- Corrected screen space computation for Canvases with Render Mode set to Screen Space – Camera and a Render Camera specified.
- Fixed order of vertices when drawing Debug Size Threshold on Quads.


Version 1.4.4 (9/26/2019)
***************************************

- Fixed an internal reporting-only bug.


Version 1.4.3 (9/19/2019)
***************************************

- Fixed a bug where AdInMoManager.IsFilled() could return false when it should actually have returned true.


Version 1.4.2 (9/8/2019)
***************************************

- Fixed an issue introduced in 1.4 that caused the AdInMo plugin to silently fail in some IL2CPP builds.
- There is now an included file Scripts/AdInMoCompatibility.cs to keep backward compatible reflected functions from getting dead-stripped in IL2CPP.


Version 1.4.1 (9/4/2019)
***************************************

- Removed checkbox from AdinmoManager that if unchecked could reduce revenue


Version 1.4 (8/22/2019)
***************************************

- If the server connection fails at app startup because it was choked out by other services, a retry will be attempted 5 seconds later.
- Added AdinmoManager.Pause() and AdInMoManager.Resume() to temporary all AdInMo processing and network traffic.
- To maximize your revenue, ads now cycle automatically every several seconds rather than waiting for you to all AdInMo.CycleTextures().
- Added AdinmoManager.IsFilled() which will return false in the rare case that no ads were filled.
- Added a camera override to each AdInMoTexture in case they are drawn with a different camera from your main scene.

Version 1.3
###########################

Version 1.3.4 (7/8/2019)
***************************************

- Removed unnecessary Asset Store folder from package.


Version 1.3.3 (7/6/2019)
***************************************

- Fixed a crash in AdinmoSender.GetServerConfig() if too many other web services are running from a game and AdInMo gets starved out.
- Fixed EncodeToPNG error occurring in certain version of Unity.
- Fixed other minor backward compatibility warnings with certain Unity versions.
- Lowest supported version is Unity now 2017.1.


Version 1.3.2 (5/10/2019)
***************************************

- Fixed coverage computation when UI.Image is used on canvas with Canvas.renderMode.WorldSpace.


Version 1.3.1 (4/9/2019)
***************************************

- Don’t show dialog with blank choices in offline mode.


Version 1.3 (3/16/2019)
***************************************

- Added AdinmoManager.ShowDialog() for extra developer revenue (see documentation).
- Improved image caching to further minimize run-time download bandwidth.
- Show API out of date message in editor window.
- Some under-the-hood bug fixes.

Version 1.2
###########################

Version 1.23.2 (1/31/2019)
***************************************

- Fixed placement size computation for images with a parent Canvas set to “Screen Space — Camera” and it’s camera is not “None”.


Version 1.23.1 (1/11/2019)
***************************************

- Fixed an editor only bug where the Scene camera was used for “Debug Size Threshold” rather than the game’s main camera when the Scene window was used.


Version 1.23 (1/2/2019)
***************************************

- AdinmoManager checkbox added for “Debug Size Threshold.” This puts a debug border around Placements. Green means it is big enough to pay for an add. Red means it’s too small.
- AdinmoManager checkbox added for “Development Build” to avoid sending impressions during test builds that checked as “Release” through Unity.
- Compatibility confirmed with Unity 2018.3.0.


Version 1.22 (12/4/2018)
***************************************

- Added checkbox to AdInMoTexture to disable rendering until the ad is ready, thus reducing the need for programming a callback.
- Strip spaces from Placement Key and Game Key to reduce cut-and-paste errors.
- Compatibility confirmed with Unity 2018.2.18.


Version 1.21 (11/16/2018)
***************************************

- Unity backward compatibility support. Tested back to Unity 5.6.1f.
- Simplified AdInMoManager prefab to a single game object with a single component.


Version 1.2 (10/16/2018)
***************************************

- Added public Color AdInMoTexture.GetBorderColor() to allow developers to choose matching backgrounds.
- Optimized network bandwidth.


Version 1.1 (9/11/2018)
***************************************

- Added offline caching mode which allows developers to continue to earn money on impressions while the player is in airplane mode or a network connection is unavailable.
- Bug fixes and performance improvements.


Version 1.0 (5/13/2018)
***************************************

- Initial Commercial Release.

