=========================
Unity Advanced Functions
=========================

From here you can add more functionality to the AdInMo SDK within your game. This section contains a range of functions that you can add and will require editing C# scripts.

Callback Function
***************************

For added control, there is a callback function you can register. This is useful for hiding an object until the replacement textures have been downloaded and applied, thus avoiding a visual pop.

.. code-block:: csharp
    :linenos:

    AdinmoManager.SetOnReadyCallback()

Your delegate function must be of the form:

.. code-block:: csharp
    :linenos:

    void MyDelegate( string message );


Extra Revenue
***************************

Between levels, you can call the function: ``AdinmoManger.ShowDialog()``. See the example scene called ExampleDialog.unity to see how to call it and register a callback to know when this dialog is done.

Please note this has been removed in AdInMo SDK 2.0.



Background Colour
***************************

If the placement doesn’t fill the object its applied to, you may need to alter the background colour. We provide a call to get the background colour set by the advertisement so you can update the object as required.

.. code-block:: csharp
    :linenos:

    Color AdinmoTexture.GetBorderColor()


Rotate Textures
***************************

To manually control when textures are cycled, call: 

.. code-block:: csharp
    :linenos:
    
    AdinmoManager.SetAutoCycleTextures(bool bEnabled)


You can then manually cycle which textures are displayed on which placements by calling the following function, e.g. between levels.

.. code-block:: csharp
    :linenos:

    void AdinmoManager.CycleTextures()


Temporarily Disable AdInMo Processes/Traffic
******************************************************

If you want to temporarily disable all AdInMo processing and network traffic, you can call:

.. code-block:: csharp
    :linenos:

    AdinmoManager.Pause()

To resume normal operations, call:

.. code-block:: csharp
    :linenos:
    
    AdinmoManager.Resume()

To check if Adinmo is currently running call:

.. code-block:: csharp
    :linenos:
    
    AdinmoManager.IsPaused()

To check if plugin’s initial setup has run, call:

.. code-block:: csharp
    :linenos:
    
    AdinmoManager.IsReady()

To check if the plugin was successfully able to connect to Adinmo’s server during initial setup (otherwise a cached setup will be loaded)


.. code-block:: csharp
    :linenos:
    
    AdinmoManager.IsSuccess()


