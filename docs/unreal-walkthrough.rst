===============================
Unreal Engine SDK Walkthrough
===============================

:Oldest Supported version of Unreal Engine: 4.26.2

Below you’ll find our handy SDK walkthrough - a step-by-step guide to using the AdInMo SDK with your game project within UE4. Further monetization tips and information including payments can be found here on our `main website <https://www.adinmo.com/>`_.

.. contents:: Table of Contents
    :depth: 2

Register with AdInMo
***************************

To begin using the AdInMo SDK, an AdInMo developer account is required.

1. Go to `AdInMo Website <https://www.adinmo.com/>`_
2. Click **Login/Sign Up button**, top-right of the page.
3. Sign up using your **email** or with your **Google account**.
4. Fill in the appropriate details and confirm your account using the instructions given. If signing up via email you will be asked to confirm your account.

Portal: Creating a Game
***************************

A **Game** is a representation of your game within AdInMo.
During this process you will create a **Game Key** that is required to work with the AdInMo SDK, **please keep this information private**.

1. `Login <https://portal.adinmo.com/#/login>`_ to your developer account on AdInMo.
2. Click the **+ Add New Game** button.
3. Fill in the details of your game - these details can be edited at any time - and click **Save**.
4. Each game has its own **game key**, this can be viewed on the details page of a game. **You will need this game key later on in the walkthrough**.
5. To view the details of your game, simply click the name of the game you want to view, either on the overview or game list page.

Portal: Creating a Placement
*******************************

This part requires at least one game on the portal.

**Placements** are simply an object which will display an advertisement, they must contain a unity 2D texture. Placements can be grouped together to show the same advertisement across different placements. One game can support multiple placements.

During this process you will create a **Placement Key** that is required to work with the AdInMo SDK, **please keep this information private**.

1. View the game you wish to create a placement for on the portal.
2. Click the, **+ Add New Placement** button, if you already have any placements they can be viewed here.
3. Fill out the details of your placement and select an aspect ratio. Placements can be edited at any time.
4. To create a group, click **New Group**. Once a group has been created it can be edited by clicking **Edit Group**. By default, a placement is set to have no group.
5. Each placement has its own **placement key**

Download AdInMo SDK
***************************

:Oldest Supported version of Unreal Engine: 4.26.2

**When updating an older version of the AdInMo SDK the best practice is to delete the existing AdInMo folder in plugins before importing the latest version of the SDK.**

**Please note that the AdInMo UE4 SDK will only work as an engine plugin or in a c++ project.**

1. Navigate to the `SDK Download <https://www.adinmo.com/sdk-download-new/>`_ page.
2. Click Download SDK.
3. Import the AdInMo SDK plugin to your UE4 project. Find the plugins folder for your project, ‘/[Project Root]/Plugins/[Plugin Name]/’ in your project files then add the AdInMo SDK folder. You may have to create a plugins folder if one doesn’t exist.
4. Open your UE4 project. Under Place Actors > Adinmo, you can find Adinmo actors to add to your level. The AdinmoUI tab displays information about your placements to help optimise your placements in the level.

Unreal Engine: AdInMo Manager
******************************
A game key is required for this part. First, the AdinmoManager needs to be added to the scene. Next, copy and paste the game key of your game into the AdinmoManager.

1. Go to Place Actors > Adinmo and place the AdinmoManager into the level.
2. Navigate back to the portal and to the appropriate game.
3. View the details of the game on the portal and copy the game key (starts with gk…).
4. In your Unreal project, paste the copied game key in the game key field in the AdinmoManager.

Unreal Engine: Adding Placements
*********************************
A placement key is required for this part.
The UE4 AdInMo SDK supports game object templates (quad) of different aspect ratios that you can add to your scene without the need to manually create them.

1. Go to Place Actors > Adinmo, here you will find quads of different aspect ratios.
2. Once you have added at least one quad, add in your placement key from the developer portal (starts with pk…).
3. Note, if you do intend to use your own mesh, make sure Allow CPU Access is enabled, and the face normal is aligned to the negative x axis.

Finished
**********
Run your game and advertisements will appear in your game.

For support, please check our `FAQ <https://www.adinmo.com/developer-faq-new/>`_.

